#include <stream9/spawn_process.hpp>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/exec.hpp>
#include <stream9/linux/fork.hpp>
#include <stream9/linux/pipe.hpp>
#include <stream9/linux/read.hpp>
#include <stream9/linux/wait.hpp>
#include <stream9/log.hpp>

namespace stream9 {

safe_integer<::pid_t, 0>
spawn_process(args const& argv)
{
    try {
        auto [in, out] = lx::pipe(O_CLOEXEC);
        auto pid = lx::fork();
        if (pid == 0) {
            in.close();

            ::execvp(argv[0], const_cast<char* const*>(argv.data()));

            //execp must be failed
            int ec = errno;
            auto n = ::write(out, &ec, sizeof(ec));
            if (n != sizeof(ec)) {
                log::err() << "spawn: execvp() failed and write() also failed to write the error code to the pipe:" << static_cast<lx::errc>(ec);
            }

            ::_exit(0);
        }
        else {
            out.close();

            int ec = 0;
            auto n = ::read(in, &ec, sizeof(ec));
            if (n == 0) { // doesn't read ec because exec succeeded.
                return static_cast<::pid_t>(pid);
            }
            else if (n == sizeof(ec)) {
                throw error { "execvp()", lx::make_error_code(ec) };
            }
            else {
                throw error { "read()", lx::make_error_code(errno) };
            }

            return static_cast<::pid_t>(pid);
        }
    }
    catch (...) {
        rethrow_error({ { "argv", argv } });
    }
}

safe_integer<::pid_t, 0>
spawn_detached_process(args const& argv)
{
    try {
        ::pid_t child_pid = 0;

        auto [in, out] = lx::pipe();
        auto [ein, eout] = lx::pipe(O_CLOEXEC);

        auto pid = lx::fork();
        if (pid == 0) { // child
            ::setsid();
            in.close();

            auto pid2 = lx::fork();
            if (pid2 == 0) { // grandchild
                ein.close();

                ::execvp(argv[0], const_cast<char* const*>(argv.data()));

                //execp must be failed
                int ec = errno;
                auto n = ::write(eout, &ec, sizeof(ec));
                if (n != sizeof(ec)) {
                    log::err() << "spawn_detached: execvp() failed and write() also failed to write the error code to the pipe:" << static_cast<lx::errc>(ec);
                }

                ::_exit(0);
            }
            else { // child
                eout.close();

                // doesn't need to wait() pid2, because this process is the session leader and will die soon so that grandchild become detached process.

                int ec = 0;
                auto n = ::read(ein, &ec, sizeof(ec));
                if (n == 0) { // couldn't read anything because exec succeeded.
                    auto n = ::write(out, &pid2, sizeof(pid2));
                    if (n != sizeof(pid2)) {
                        log::err() << "spawn_detached: failed to write() the PID of the child process to the pipe: n =" << n << ", errno =" << static_cast<lx::errc>(errno);
                    }

                    ::_exit(0);
                }
                else if (n == sizeof(ec)) { // exec failed and ec holds error code.
                    ::_exit(ec);
                }
                else {
                    log::err() << "spawn_detached: failed to read() the error code from the pipe: n =" << n << ", errno =" << static_cast<lx::errc>(errno);
                    ::_exit(0);
                }
            }
        }
        else { // parent
            out.close();
            ein.close();
            eout.close();

            int status = 0;
            lx::waitpid(pid, &status);
            auto ec = WEXITSTATUS(status);
            if (ec > 0) {
                throw error { "execvp()", lx::make_error_code(ec) };
            }
            else {
                auto n = lx::read(in, child_pid).or_throw();
                if (n != sizeof(child_pid)) {
                    throw error { "read() PID from pipe",
                        lx::make_error_code(errno), {
                            { "n", n },
                        }
                    };
                }
            }
        }

        return child_pid;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9

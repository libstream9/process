#ifndef STREAM9_PROCESS_SPAWN_HPP
#define STREAM9_PROCESS_SPAWN_HPP

#include <sys/types.h>

#include <stream9/args.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9 {

safe_integer<::pid_t, 0>
spawn_process(args const&);

safe_integer<::pid_t, 0>
spawn_detached_process(args const&);

} // namespace stream9

#endif // STREAM9_PROCESS_SPAWN_HPP
